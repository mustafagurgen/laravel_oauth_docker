<?php
/**
 * This cron will run every day on 9:00 AM
 * crontab -e 
 * 0 9 * * * /usr/bin/php ./crons/cron.php 
 */
// 

$params=['limit'=>10, 'wc_date'=>date("Y-m-d")];
$defaults = array(
CURLOPT_URL => 'http://localhost/api/v1/crons/emailing',
CURLOPT_POST => true,
CURLOPT_POSTFIELDS => $params,
);
$ch = curl_init();
curl_setopt_array($ch, ($options + $defaults));

<?php

namespace App\Traits;

use \Illuminate\Http\Request;

use Validator;

trait RequestTrait
{

	public function getPrm()
  {
    $request = new Request();
    return $request->json()->all();
  }
  
  public function resultOk($message,$data)
  {
    return response()->json(['status' => 1, "message" => $message, "data" => $data], 200); 
  }

  public function resultWarning($message,$data)
  {
    return response()->json(['status' => 2, "message" => $message, "data" => $data], 200); 
  }


  public function resultError($error,$data)
  {
    return response()->json(['status' => -1, "error" => $error, "data" => $data], 401); 
  }

  public function logError($error)
  {
    // A logger can be added to this
    return true; 
  }

  public function logResult($result)
  {
    // A logger can be added to this
    return true; 
  }

}

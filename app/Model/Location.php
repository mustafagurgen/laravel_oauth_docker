<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Location extends Model
{
    //
    public function weathercasts() { 
        return $this->hasMany("App\Model\WeatherCast", "loc_id")->select(DB::raw('loc_id,wc_date,wc_status,wc_icon,wc_night,wc_day,wc_date'))->orderBy('id','DESC')->limit(7);
    }

    public function weathercast() { 
        return $this->hasOne("App\Model\WeatherCast", "loc_id")->select(DB::raw('loc_id,wc_date,wc_status,wc_icon,wc_night,wc_day,wc_date'))->orderBy('id','DESC')->limit(7);
    }
}

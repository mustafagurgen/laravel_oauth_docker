<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserNotification extends Model
{
    public function location() { 
        return $this->hasMany("App\Model\Location", "id");
    }
    
    public function weathercast() { 
        return $this->hasOne("App\Model\WeatherCast", "loc_id")->select(DB::raw('loc_id,wc_date,wc_status,wc_icon,wc_night,wc_day,wc_date'))->orderBy('id','DESC');
    }

    public function user() { 
        return $this->hasOne("App\User", "id")->select(DB::raw('id,name,email,user_type'));
    }

}

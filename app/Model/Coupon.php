<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Coupon extends Model
{
    public function user() { 
        return $this->hasOne("App\User", "user_id")->select(DB::raw('id,name,email,user_type'));
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WeatherCast extends Model
{
    public function location() { 
        return $this->hasMany("App\Model\Location", "loc_id");
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\UserNotification;
use App\Traits\RequestTrait;
use Validator;
use Exception;
use Illuminate\Support\Facades\DB;

class CronJobController extends Controller
{
    use RequestTrait; // Request params function, response function (success or fail)
    
    public function sendEmail($data) {
         try {
            // Emailer::send($data) // toplu email gönderimi yapacak bir Model eklenecek
            $this->logResult("Email has been sent");
            return true;
         }
         catch (Exception $e) {
            $this->logError("an error occured while sending emails");
            return false;
         }
    }

    public function getNotifications() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
                'wc_date' => 'required|string',
                'limit' => "required|integer|max:100"
            ]);

            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('wc_date'))  return $this->resultError("You must enter a valid date!", null);
                if ($err->first('limit'))  return $this->resultError("You must enter a limit for sending email!", null);
            }

            $nt =  UserNotification::select(DB::raw('id,user_id,loc_id'))->with(['weathercast' => function($q) use ($request) {
                return $q->where('wc_date',$request['wc_date']);
            }])->with('user')->with('location')->where('specs',0)->limit($request['limit'])->get();

            if (!$nt) {
                $this->logResult("There is no data to send email");
                return $this->resultWarning('There is no data to send email',$nt);
            }

            // Send email
            $this->sendEmail($nt);

            // The lines below can be uncomment to see the result manually
            // return $this->resultWarning('Email has been sent',$nt);

        }
        catch (Exception $e) {
            $this->logError("Sending Email Error: ".$e->getMessage());
            return $this->resultError("An error occured while listing!", $e->getMessage());
        }
    }
}

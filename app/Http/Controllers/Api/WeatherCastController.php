<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Location;
use App\Traits\RequestTrait;
use App\Model\WeatherCast;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WeatherCastController extends Controller
{
    use RequestTrait;

    public function addWeather() {
        try {

            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
            'loc_id' => 'required|integer',
            'wc_status' => 'required|string|max:80',
            'wc_day' => 'required|string|max:20',
            'wc_night' => 'required|string|max:20',
            'wc_icon' => 'required|string|max:20',
            'wc_date' => 'string',
            ]);
    
            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('loc_id'))  return $this->resultError("Please select a location!", null);
                if ($err->first('wc_status'))  return $this->resultError("Weather status must be enter!", null);
                if ($err->first('wc_day'))  return $this->resultError("Please insert weather day degree!", null);
                if ($err->first('wc_night'))  return $this->resultError("Please insert weather night degree!", null);
                if ($err->first('wc_icon'))  return $this->resultError("Please insert weather icon degree!", null);
            }

            $wc = new WeatherCast();
            $wc->loc_id = (int) $request['loc_id'];
            $wc->wc_status = $request['wc_status'];
            $wc->wc_day = $request['wc_day'];
            $wc->wc_night = $request['wc_night'];
            $wc->wc_icon =  $request['wc_icon'];
            $wc->wc_date =  $request['wc_date'];
            $wc->save();

            return $this->resultOk('You have successfuly add weather cast',$wc);
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while adding weather cast!", $e->getMessage());
        }
    }

    public function listWeather() {
        try {
            $user = Auth::user();
            if ($user){

                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'slug' => 'required|string'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('slug'))  return $this->resultError("You must enter a location!", null);
                }

                $weather =  Location::select(DB::raw('id,city,slug,postal_code'))->with('weathercasts')->where('slug',$request['slug'])->get();

                return $this->resultOk('List waether cast for given location',$weather);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }

    public function getWeather() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
            'slug' => 'required|string',
            'wc_date' => 'required|string',
            ]);

            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('slug'))  return $this->resultError("You must enter a location!", null);
                if ($err->first('wc_date'))  return $this->resultError("You must enter a date!", null);
            }

            $weather =  Location::select(DB::raw('id,city,slug,postal_code'))->with(['weathercast' => function($q) use ($request) {
                return $q->where('wc_date',$request['wc_date']);
            }])->where('slug',$request['slug'])->get();

            return $this->resultOk('List waether cast for given location',$weather);

        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }
}

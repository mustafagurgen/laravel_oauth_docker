<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\UserNotification;
use App\Traits\RequestTrait;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserNotificationController extends Controller
{
    use RequestTrait; // Request params function, response function (success or fail)

    public function addNotification() {
        try {

            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
            'loc_id' => 'required|integer',
            'user_id' => 'required|integer',
            ]);
    
            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('loc_id'))  return $this->resultError("Please select a location!", $err);
                if ($err->first('user_id'))  return $this->resultError("There is no related user!", $err);
            }

            $nt = new UserNotification();
            $nt->loc_id = (int) $request['loc_id'];
            $nt->user_id = (int) $request['user_id'];
            $nt->save();

            return $this->resultOk('You have successfully add notification',$nt);
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while adding notification!", $e->getMessage());
        }
    }

    public function listNotifications() {
        try {
            $user = Auth::user();
            if ($user){
                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'start' => 'integer'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('start'))  return $this->resultError("The value you have entered must be integer!", null);
                }

                $nt =  UserNotification::select(DB::raw('id,loc_id,user_id'))->with('location')->where('user_id',$user->id)->get();

                return $this->resultOk('List waether cast for given location',$nt);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }

    public function getNotification() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
                'user_id' => 'required|integer',
                'wc_date' => 'required|string',
            ]);

            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('user_id'))  return $this->resultError("You must enter a valid integer value!", null);
                if ($err->first('wc_date'))  return $this->resultError("You must enter a valid date!", null);
            }

            $nt =  UserNotification::select(DB::raw('id,user_id,loc_id'))->with(['weathercast' => function($q) use ($request) {
                return $q->where('wc_date',$request['wc_date']);
            }])->with('user')->with('location')->where('user_id',$request['user_id'])->where('specs',0)->get();

            return $this->resultOk('List waether cast for given location',$nt);

        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing locations!", $e->getMessage());
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Coupon;
use App\Traits\RequestTrait;
use Validator;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class CouponController extends Controller
{
    use RequestTrait; // Request params function, response function (success or fail)

    public function addCoupon() {
        try {
            $user = Auth::user();
            if ($user){
            
                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'coupon_code' => 'required|string|max:15|unique:coupons',
                'desc' => 'string|max:155',
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('coupon_code'))  return $this->resultError("Coupon code can not be empty and must be unique!", $err);
                    if ($err->first('desc'))  return $this->resultError("Description must be lower than 156 characters!", $err);
                }

                $cpn = new Coupon();
                $cpn->coupon_code = $request['coupon_code'];
                $cpn->desc = $request['desc'];
                $cpn->save();

                return $this->resultOk('You have successfully add coupon',$cpn);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while adding notification!", $e->getMessage());
        }
    }

    public function listCoupons() {
        try {
            $user = Auth::user();
            if ($user){
                $request = $this->getPrm();
                $validator = Validator::make($request, 
                [ 
                'start' => 'integer'
                ]);
        
                if ($validator->fails()) {
                    $err = $validator->errors();
                    if ($err->first('start'))  return $this->resultError("The value you have entered must be integer!", null);
                }

                $cpn =  Coupon::select(DB::raw('id,coupon_code,`desc`'))->limit(50)->offset($request['start'])->orderBy('id','DESC')->get();

                return $this->resultOk('List coupons',$cpn);
            }
            else {
                return $this->resultError('Unauthorised', null);
            }
        }
        catch (Exception $e) {
            return $this->resultError("An error occured while listing coupons!", $e->getMessage());
        }
    }

    public function getCoupon() {
        try {
            $request = $this->getPrm();
            $validator = Validator::make($request, 
            [ 
                'coupon_id' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $err = $validator->errors();
                if ($err->first('coupon_id'))  return $this->resultError("You must enter a valid integer value!", null);
            }

            $cpn =  Coupon::select('*')->where('id',$request['coupon_id'])->first();

            return $this->resultOk('Get coupon details',$cpn);

        }
        catch (Exception $e) {
            return $this->resultError("An error occured while getting details about coupon!", $e->getMessage());
        }
    }
}

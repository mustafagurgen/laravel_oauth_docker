## Aşağıdaki komutlar bir defaya mahsus çalışırılarak composer yüklenmesi ve larave projesinin oluşturulması sağlandı
docker run --rm -v $(pwd):/app composer install
docker run --rm -v $(pwd):/app composer create-project --prefer-dist laravel/laravel teknasyon

## Harici klasörler
  # apitest 
    - http post metodu requestlerin yapıldığı örneklemelerin yer aldığı klasördür.
  # crons
    - crontab ile çalıştırılacak olan scriptlerin(php veya sh) yer aldığı klasördür
  # mysql
    - mysql-server'ın çalışma dosyalarının (data ve config) mount edildiği klasördür
  # php
    - php-fpm'in ini dosyasının mount edildiği klasördür
  # nginx
    - web server config dosyasının mount edildiği klasördür

## Docker Compose ile ilgili servislerin (nginx, mysql, php-fpm ve composer kurulumu)
  # docker-compose.yml dosyasında ilgili servisler yapılandırıldı.
  - nginx klasöründe nginx ile ilgili config ayarları yapılabilir
  - mysql klasöründe veritabanı data dosyaları ve config ayarları yapılabilir
  - php klasöründe php.ini ayaları eklenebilir 

  - docker-compose up -d ile proje çalıştırılabilir

## Mysql Queries
  - ORM ilişkisel veritabanı sorguları kullanıldı.
  - Elequent sorgular kullanılmadı. Sadece ORM sorgulardaki tablo alanlarının getirilmesi için kulanıldı. 

## Modal 
  # app/Modal
    -- default oluşturulan User modal doyası hariç diğer oluşturulan dosyalar app/Modal altında oluşturuldu 
    
    docker-compose exec app php artisan make:model Model/Location -m (-m migration dosyasının otomatik oluşturulması için)

## Controller 
  # app/Http/Controllers/Api
    -- Rest Api için oluşturulan dosyalar app/Http/Controllers/Api altında oluşturuldu 
    docker-compose exec app php artisan make:Controller Api/UserCouponController

## Crons
  crontab -e  komutundan sonra aşağıdaki satır eklenebilir.
  0 9 * * * /usr/bin/php ./crons/cron.php #php dosyasının tam yolu yazılmalı 

## Oauth 
  - laravel/passport ile oauth2 ile aauthentcation sağlandı.
  - token expire süresi 15 dk olarak ayarlandı. Değiştirilmek istenirse;
    ./app/Providers/AuthServiceProvider.php dosyasında ki "    public function boot() " metodu içinde değiştirilebilir.

    docker-compose exec app php artisan passport:install


## Request ve Response Standartlaştırılması
  ## Traits
  # app/Traits/RequestTrait.php 
    - request parametrelerinin tum projede standart bir şekilde kullanılması getPrm () metodu
    - response: 3 farklı şekilde kullanıldı ve tüm projede kullanıldı (fonksiyon olaran);
      - resultOk: İşlemler başarılı olduğunda döndürülecek sonuç
      - resultError: İşlemler başarısız olduğunda döndürülecek sonuç
      - resultWarning: İşlemler başarılı olup da bilgilendirme veya uyarma amaçlı döndürülecek sonuç

## Database 
  # Migrations
    - database klasöründeki migration dosyaları ile tablolar oluşturulabilir.
    docker-compose exec app php artisan migrate

## Git Komutları
  # Git on existing directory
  git add .
  git remote add origin https://mustafagurgen@bitbucket.org/mustafagurgen/challenge.git
  git pull origin master


  # .gitignore
    - Güvenlik ve gereksiz repo dosya büyüklüğünü engellemek için .env ve storage gibi dataların config ve data dosyalarının tutulduğu dosya ve klasörler .gitignore dosyasına eklenerek git reposuna gönderilmesi engellendi.

  # first commit
  git add .
  git commit -m "Creating project and setting .env files"
  git push origin master


  # Merge
  git checkout master
  git merge new-modules
  git branch -d new-modules


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->integer('specs')->unsigned()->default(1)->comment('0: Pasif, 1:Aktif');
            $table->integer('country')->unsigned()->default(1)->comment('1: Turkey');
            $table->string('city',155);
            $table->string('slug',155);
            $table->string('latitude',80)->nullable();
            $table->string('longitude',80)->nullable();
            $table->string('postal_code',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
